----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.10.2020 15:50:47
-- Design Name: 
-- Module Name: AES_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
library numeric_std;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AES_top_tb is

end entity AES_top_tb;

architecture AES_top_tb_arch of AES_top_tb is

  component AES_top is
    port (
      clock_i  : in  std_logic;
      resetb_i : in  std_logic;
      start_i  : in  std_logic;
      sel_i    : in  std_logic_vector (3 downto 0);
      data_o   : out std_logic_vector (7 downto 0));
      --done_o   : out std_logic);
  end component AES_top;

  signal clock_s  : std_logic;
  signal resetb_s : std_logic;
  signal start_s  : std_logic;
  signal sel_s    : std_logic_vector (3 downto 0);
  signal data_s   : std_logic_vector (7 downto 0);
  signal done_s   : std_logic;

begin  -- architecture AES_top_tb_arch

  DUT : AES_top
    port map (
      clock_i  => clock_s,
      resetb_i => resetb_s,
      start_i  => start_s,
      sel_i    => sel_s,
      data_o   => data_s);
     -- done_o   => done_s);

   P1:process
   begin
    clock_s <= '0';
    wait for 10 ns;
    clock_s <= '1';
    wait for 10 ns;
end process;

  resetb_s <= '1', '0'    after 15 ns;
  start_s <= '0','1' after 25 ns;
  sel_s <= "0000","0001" after 500 ns, "0010" after 550 ns;

end architecture AES_top_tb_arch;