----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.10.2020 15:50:47
-- Design Name: 
-- Module Name: AES_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.crypt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AES_top is
    Port ( clock_i : in STD_LOGIC;
           resetb_i : in STD_LOGIC;
           start_i : in STD_LOGIC;
           sel_i : in STD_LOGIC_VECTOR (3 downto 0);
           data_o : out STD_LOGIC_VECTOR (7 downto 0));
           --done_o : out STD_LOGIC);

end AES_top;



architecture Behavioral of AES_top is

  component AES is
  port (
    clock_i    : in  std_logic;
    reset_i    : in  std_logic;
    start_i    : in  std_logic;
    key_i      : in  bit128;
    data_i     : in  bit128;
    data_o     : out bit128;
    aes_done_o : out std_logic);
end component AES;

component clk_wiz_0
port
 (-- Clock in ports
  -- Clock out ports
  clk_out1          : out    std_logic;
  clk_in1           : in     std_logic
 );
end component;

signal clock_s    : std_logic;
signal clock_50MHz_s : std_logic;
signal reset_s    : std_logic;
signal start_s    : std_logic;
signal key_s      : bit128;
signal data_i_s     : bit128;
signal data_o_s     : bit128;
signal aes_done_s : std_logic;

begin

clock_50MHz : clk_wiz_0
   port map ( 
  -- Clock out ports  
   clk_out1 => clock_50MHz_s,
   -- Clock in ports
   clk_in1 => clock_i
 );

  AES0:AES
    port map (
      clock_i    => clock_50MHz_s,
      reset_i   => resetb_i,
      start_i    => start_i,
      key_i      => key_s,
      data_i     => data_i_s,
      data_o     => data_o_s,
      aes_done_o => open);

   data_o <= data_o_s((to_integer((unsigned(sel_i)))*8 +7) downto to_integer((unsigned(sel_i)))*8);
   key_s <=  x"2b7e151628aed2a6abf7158809cf4f3c";
   data_i_s <=  x"526573746f20656e2076696c6c65203f";

end Behavioral;


